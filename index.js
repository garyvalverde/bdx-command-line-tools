#!/usr/bin/env node --harmony
const { program } = require('commander');
const pkg = require('./package.json');

const {
  code, short, npmHome, scaffolding, session,
} = require('./bin');

(() => {
  program.version(pkg.version);
  program.description('A set of tools to work on the BDX Live App');
  code(program);
  short(program);
  session(program);
  npmHome(program);
  scaffolding(program);
  program.parse(process.argv);
})();
