## 1.5.4 (07/19/2021)

- Pin dependencies to avoid problems with deprecated functionality (mainly from yeoman)

## 1.5.3 (07/08/2021)

- Allow setting the filename on the bdx session command (default to environment)

## 1.5.2 (07/07/2021)

- Include -t and -a values in token to get the token and access token at will

## 1.5.1 (07/02/2021)

- Include default params on global to solve issues with nested combo box

## 1.5.0 (06/21/2021)

- Update NPM dependencies
- New session method to generate mock stores
- Little class refactoring

## 1.4.1 (09/19/19)

### BugFix

- Replace css importing so it uses unsafeCSS directive

## 1.4.0 (09/19/19)

### Feature

- Add support for decorators (customElements and property) by default

### BugFix

- Exports elements by default
- Replace @polymer/lit-element with lit-element (due to lts upgrate)
- Move HTML templates into named exports and include JSDoc to link the @this binding

## 1.3.2 (07/26/19)

### Feature

- Optional advanced comments

## 1.3.1 (07/26/19)

### Feature

- Base comments for element sections

## 1.3.0 (03/20/19)

### Feature

- Omit `bdx-` from the file / folder name

### BugFix

- Solve linting problems

## 1.2.2 (02/14/19)

### BugFix

- Change property types into new notation

## 1.2.1 (02/14/19)

### BugFix

- Solve problem with template extensions

## 1.2.0 (02/14/19)

### Feature

- Migrate template from `.css.js` into simple `.css` due to webpack implementation

## 1.1.0 (11/07/18)

### BugFixes

- Migrate template from `lit-element@0.5.2` into `lit-element@0.6.2`
