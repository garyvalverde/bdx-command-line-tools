# BDX Live Command Line Tools
A set of tools to work on the redesign app


# Install
```
npm install -g bitbucket:garyvalverde/bdx-command-line-tools
```

# Usage
```
Usage: index [options] [command]

A set of tools to work on the BDX Live App

Options:
  -V, --version               output the version number
  -h, --help                  display help for command

Commands:
  code [options] [directory]
  short [url]
  session
  nh [options] [name]
  add
  help [command]              display help for command
```