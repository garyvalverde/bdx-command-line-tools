/**
 * Importings
 */
const { prompt, promptList } = require('../utils');
const { getPromps: sessionGetPromps, getPrompsPopulated: sessionGetPrompsPopulated } = require('./config/session');
const { getEnvironment, getPromps: generatorsGetPromps } = require('./config/generators');
const { generateToken } = require('../utils/generate-token');
const { login } = require('./session/login');

/**
 * Run the given generator. If no Yeoman environment is provided, a new one
 * will be created. If the generator does not exist in the environment, an
 * error will be thrown.
 */
async function runGenerator(id) {
  return new Promise((resolve, reject) => {
    getEnvironment().run(id, {}, (error) => (error ? reject(error) : resolve()));
  }).catch((err) => console.log(err));
}

/**
 * Prompt the user to select a generator. When the user
 * selects a generator, run it.
 */
module.exports.promptGeneratorSelection = async function promptGeneratorSelection() {
  await runGenerator(await promptList(generatorsGetPromps()));
};

/**
 * Run the given generator. If no Yeoman environment is provided, a new one
 * will be created. If the generator does not exist in the environment, an
 * error will be thrown.
 */
async function runLogin({ environment, username, password, filename } = {}, opts) {
  await login(`http://${environment}-api.thebdxlive.com/Bdx/`, generateToken({ username, password }), { filename }, opts);
}

/**
 * Prompt the user to select a generator. When the user
 * selects a generator, run it.
 */
module.exports.promptSessionLogin = async function promptSessionLogin(opts) {
  const answers = await prompt(sessionGetPromps());
  const { filename } = await prompt(sessionGetPrompsPopulated(answers));
  await runLogin({ ...answers, filename }, opts);
};
