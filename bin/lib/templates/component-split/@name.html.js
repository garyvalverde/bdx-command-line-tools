/**
 * Component Base Elements
 */
import { html } from 'lit-element';

/**
 * @this {import('./<%= fileName %>.js').<%= className %>}
 */
export function template() {
  return html`
    <h1>Hello ${this.name}</h1>
  `;
}
