import { <%= changeCase.constantCase(name) %> } from './types';

export const <%= changeCase.camelCase(name) %>State = () => ({ type: <%= changeCase.constantCase(name) %> });
