// Partner Sagas
// These are Ajax Calls (side effects handled with Saga Middleware) to the Api
import { takeEvery, effects } from 'redux-saga';
import { request<%= changeCase.camelCase(name) %> } from './api';
import { <%= changeCase.constantCase(name) %>_REQUEST } from './types';

import { <%= changeCase.camelCase(name) %>Success, <%= changeCase.camelCase(name) %>Fail } from './actions';

function* get<%= changeCase.camelCase(name) %>() {
  try {
    const response = yield effects.call(request<%= changeCase.camelCase(name) %>);
    // not a valid user
    if (response && response.StatusCode === 'Error') {
      throw Error(response.Result.Errors[0].message); // TODO: Change it with Marlon
    }

    yield effects.put(<%= changeCase.camelCase(name) %>Success(response.data));
  } catch (error) {
    // TODO Logic to identify errors from the api and show
    //  the proper cause to the user in the login page
    yield effects.put(<%= changeCase.camelCase(name) %>Fail(error));
  }
}

export const <%= changeCase.camelCase(name) %>Sagas = [
  takeEvery(<%= changeCase.constantCase(name) %>_REQUEST, get<%= changeCase.camelCase(name) %>),
];
