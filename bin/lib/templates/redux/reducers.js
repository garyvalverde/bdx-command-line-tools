import { produce } from 'immer';
import { <%= changeCase.constantCase(name) %> } from './types';

const DEFAULT_STATE = {};

export const <%= changeCase.camelCase(name) %>Reducer = (state = DEFAULT_STATE, action) => produce(state, (draft) => {
  switch (action.type) {
    case <%= changeCase.constantCase(name) %>:
      draft.<%= changeCase.camelCase(name) %> = action.data;
  }
});
