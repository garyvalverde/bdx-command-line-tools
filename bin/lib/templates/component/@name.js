/**
 * Helper Importings
 */
import { LitElement, html, css, customElement, property } from 'lit-element';<% if (connected) { %>
import { connect } from 'pwa-helpers/connect-mixin';
import { store } from '@/app/app.store';<% } %>

/**
 * Element Definition
 */
@customElement('<%= name %>')
export class <%= className %> extends <%= connected ? 'connect(store)(LitElement)' : 'LitElement' %> {
  @property({ type: String })
  name = '<%= className %>';

  /**
   * Applies styling to the element shadowRoot using the `static get styles`
   * property. Styling will apply using `shadowRoot.adoptedStyleSheets` where
   * available and will fallback otherwise. When Shadow DOM is polyfilled,
   * ShadyCSS scopes styles and adds them to the document. When Shadow DOM
   * is available but `adoptedStyleSheets` is not, styles are appended to the
   * end of the `shadowRoot` to [mimic spec
   * behavior](https://wicg.github.io/construct-stylesheets/#using-constructed-stylesheets).
   */
  static get styles() {
    return css`
      h1 {
        font-size: 20px;
        color: blue;
        text-align: center;
      }
    `;
  }

  /**
   * Invoked on each update to perform rendering tasks. This method must return
   * a lit-html TemplateResult. Setting properties inside this method will *not*
   * trigger the element to update.
   * @returns {TemplateResult} Must return a lit-html TemplateResult.
   */
  render() {
    return html`
      <h1>Hello ${this.name}</h1>
    `;
  }
<% if (comments) { %>
  // ======================== Element Getters - Start ============================ //
  // ======================== Element Getters - End ============================== //

  // ======================== Redux Requests - Start ============================= //
  // ======================== Redux Requests - End =============================== //

  // ======================== Lifecycle Callbacks - Start ======================== //<% } %><% if (connected) { %>
  /**
   * Called after the Redux state changes. Implement to capture this
   * changes and do something with them on the component
   * @param _state Current state after the change
   */
  stateChanged() {}<% } %>
<% if (comments) { %>
  // ======================== Lifecycle Callbacks - End ========================== //

  // ======================== Listener Callbacks - Start ========================= //
  // ======================== Listener Callbacks - End =========================== //
<% } %>}
