const ViewGenerator = require('./view');
const ElementGenerator = require('./element');
const ReduxGenerator = require('./redux');

module.exports = { ElementGenerator, ViewGenerator, ReduxGenerator };
