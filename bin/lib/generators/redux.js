const path = require('path');
const { GlobalGeneratorBase } = require('./GlobalGeneratorBase');

const BDX_ACTION = 'actions.js';
const BDX_REDUCER = 'reducers.js';
const BDX_REDUX = 'redux-folder';
const BDX_SAGA_API = 'sagas.js';
const BDX_TYPE = 'types.js';

const reduxObjects = [{
  name: 'Actions',
  value: BDX_ACTION,
}, {
  name: 'Types',
  value: BDX_TYPE,
}, {
  name: 'Reducers',
  value: BDX_REDUCER,
}, {
  name: 'API Saga',
  value: BDX_SAGA_API,
}, {
  name: 'Redux Folder',
  value: BDX_REDUX,
}];

class ReduxGenerator extends GlobalGeneratorBase {
  /**
   * This is necessary to prevent an exception in Yeoman when creating
   * storage for generators registered as a stub and used in a folder
   * with a package.json but with no name property.
   * https://github.com/Polymer/polymer-cli/issues/186
   */
  rootGeneratorName() {
    return this.properties.generatorName;
  }

  get properties() {
    return {
      generatorName: 'ReduxGenerator',
      prompts: [{
        type: 'list',
        name: 'type',
        message: 'Redux Element Kind',
        choices: reduxObjects,
      }, {
        type: 'input',
        name: 'name',
        message: 'Item Name',
        default: this.appname,
      }],
    };
  }

  /**
   * This is the specific generation process for the Generator Base
   */
  async writing() {
    const WITHIN_REDUX = this.destinationPath().includes('@redux');
    // Set Roots
    this.sourceRoot(this.templatePath(__dirname, '../templates/redux'));
    this.destinationRoot(WITHIN_REDUX ? this.destinationPath() : this.destinationPath('@redux'));
    this.props.parentName = path.basename(this.destinationPath('../'));

    // Kinds
    const SINGLE = [BDX_ACTION, BDX_REDUCER, BDX_SAGA_API, BDX_TYPE];
    const GROUPS = [BDX_REDUX];

    const TYPE = this.props.type;
    if (SINGLE.includes(TYPE)) {
      this._copyFile(TYPE);
    } else if (GROUPS.includes(TYPE)) {
      const { files } = await this._prompt([{
        type: 'checkbox',
        name: 'files',
        choices: SINGLE,
        default: SINGLE,
      }]);
      this._copyList(files);
    }
  }

  /**
   * Some boilerplate required because Yeoman Guys aren't nice :(
   * https://is.gd/6QtdVN
   */

  async prompting() { await super.prompting(); }

  initializing() { super.initializing(); }

  end() { super.end(); }
}

module.exports = ReduxGenerator;
