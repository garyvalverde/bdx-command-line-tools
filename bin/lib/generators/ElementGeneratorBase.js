const { GlobalGeneratorBase } = require('./GlobalGeneratorBase');
const FindRelative = require('../../utils/find-relative');

class ElementGeneratorBase extends GlobalGeneratorBase {
  /**
   * This is the specific generation process for the Generator Base
   */
  async writing() {
    this.sourceRoot(this.templatePath(__dirname, '../templates', this.props.separed ? 'component-split' : 'component'));
    this.destinationRoot(this.destinationPath(this.props.separed ? this.props.fileName : ''));
    this.props.findRelative = new FindRelative(this.destinationPath());

    const {
      fileName,
      separed,
    } = this.props;
    const extensions = separed ? ['.js', '.html.js', '.css'] : ['.js'];

    extensions.forEach((ext) => {
      this.fs.copyTpl(
        this.templatePath(`@name${ext}`),
        this.destinationPath(`${fileName}${ext}`),
        this.props,
      );
    });
  }
}

module.exports = { ElementGeneratorBase };
