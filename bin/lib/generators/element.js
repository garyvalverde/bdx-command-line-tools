const validateElementName = require('validate-element-name');
const { ElementGeneratorBase } = require('./ElementGeneratorBase');

class ElementGenerator extends ElementGeneratorBase {
  /**
   * This is necessary to prevent an exception in Yeoman when creating
   * storage for generators registered as a stub and used in a folder
   * with a package.json but with no name property.
   * https://github.com/Polymer/polymer-cli/issues/186
   */
  rootGeneratorName() {
    return this.properties.generatorName;
  }

  get properties() {
    return {
      generatorName: 'ElementGenerator',
      prompts: [{
        type: 'input',
        name: 'name',
        message: 'Element Name',
        default: this.appname,
        validate: (name) => {
          const nameValidation = validateElementName(name);

          if (!nameValidation.isValid) {
            this.log(`\n${nameValidation.message}\nPlease try again.`);
          } else if (nameValidation.message) {
            this.log('');
          }

          return nameValidation.isValid;
        },
      }, {
        type: 'confirm',
        name: 'connected',
        message: 'Is connected?',
        default: false,
      }, {
        type: 'confirm',
        name: 'separed',
        message: 'Is HTML / CSS / JS separed?',
        default: false,
      }, {
        type: 'confirm',
        name: 'comments',
        message: 'Include advanced comments?',
        default: false,
      }],
      computedProps: (props) => ({
        className: props.name.replace(/(^|-)(\w)/g, (_match, _p0, p1) => p1.toUpperCase()),
        fileName: props.name.replace(/bdx-/gi, '').toLowerCase(),
      }),
    };
  }

  /**
   * Some boilerplate required because Yeoman Guys aren't nice :(
   * https://is.gd/6QtdVN
   */
  async writing() { await super.writing(); }

  initializing() { super.initializing(); }

  async prompting() { await super.prompting(); }

  end() { super.end(); }
}

module.exports = ElementGenerator;
