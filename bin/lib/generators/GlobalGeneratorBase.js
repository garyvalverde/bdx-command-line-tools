const chalk = require('chalk');
const assign = require('lodash/assign');
const Generator = require('yeoman-generator');
const changeCase = require('change-case');

class GlobalGeneratorBase extends Generator {
  /**
   * Allows the generator to be intialized correctly
   * you can add default properties here
   *
   * @param {*} args
   * @param {*} opts
   */
  constructor(args, opts) {
    super(args, opts);
    this.props = {};
  }

  /**
   * This function runs when the generator is initializing
   */
  initializing() {
    this.appname = this.appname.replace(/\s+/g, '-');
  }

  /**
   * This function generates the prompts showed to the users in order to configure
   * a generator settings
   */
  async prompting() {
    this.props = await this._prompt();
    assign(
      this.props,
      this.properties.additionalProps || {},
      this.properties.computedProps ? this.properties.computedProps(this.props) : {},
    );
  }

  /**
   * This functions runs at the end of the generation process
   */
  end() {
    this.log(chalk.bold('\nSetup Complete!'));
  }

  async _prompt(config) {
    return this.prompt(config || this.properties.prompts);
  }

  _copyList(files) {
    files.forEach((file) => {
      this.fs.copyTpl(
        this.templatePath(file),
        this.destinationPath(file),
        {
          changeCase, ...this.props,
        },
      );
    });
  }

  _copyAll() {
    this.fs.copyTpl(
      this.templatePath(),
      this.destinationPath(),
      {
        changeCase, ...this.props,
      },
    );
  }

  _copyFile(template) {
    this.fs.copyTpl(
      this.templatePath(template),
      this.destinationPath(template),
      {
        changeCase, ...this.props,
      },
    );
  }
}

module.exports = { GlobalGeneratorBase };
