/**
 * Importings
 */
const chalk = require('chalk');
const YeomanEnvironment = require('yeoman-environment');

/**
 * Local Generators
 */
const { ElementGenerator, ViewGenerator, ReduxGenerator } = require('../generators');

const GENERATORS = [{
  id: 'bdxlive:element',
  name: 'ElementGenerator',
  description: 'A new component',
  generator: ElementGenerator,
}, {
  id: 'bdxlive:view',
  name: 'ViewGenerator',
  description: 'A new view',
  generator: ViewGenerator,
}, {
  id: 'bdxlive:redux',
  name: 'ReduxGenerator',
  description: 'A new redux element',
  generator: ReduxGenerator,
}];

/**
 * Create & populate a Yeoman environment.
 */
function getEnvironment() {
  const ENV = new YeomanEnvironment();
  GENERATORS.forEach((gen) => {
    ENV.registerStub(gen.generator, gen.id);
  });
  return ENV;
}

/**
  * Create the prompt used for selecting which template to run. Generate
  * the list of available generators by filtering relevent ones out from
  * the environment list.
  */
function getPromps() {
  return {
    message: 'Which starter template would you like to use?',
    choices: GENERATORS.map(({ id, name, description }) => ({
      name: chalk.dim(`${name} - ${description}`),
      value: id,
      short: name,
    })),
  };
}

module.exports = { getEnvironment, getPromps };
