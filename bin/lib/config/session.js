/**
 * Create the prompt used for selecting which template to run. Generate
 * the list of available generators by filtering relevent ones out from
 * the environment list.
 */
function getPromps() {
  return [
  {
    type: 'environment',
    name: 'environment',
    message: 'Environment',
    default: 'redesign-build',
  },
  {
    type: 'input',
    name: 'username',
    message: 'Username',
    default: 'gvbuilder',
  },
  {
    type: 'input',
    name: 'password',
    message: 'Password',
    default: 'password',
  }];
}

function getPrompsPopulated({ environment }) {
  return [
  {
    type: 'input',
    name: 'filename',
    message: 'Filename',
    default: environment,
  }];
}

module.exports = { getPromps, getPrompsPopulated };
