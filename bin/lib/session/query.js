module.exports.userInfoQuery = (userId) => `query {
  users(userId:${userId}) {
    userId
    userName
    firstName
    lastName
    email
    roleGroup
    roleName
    roleId
    showWelcomeScreen
    dateCreated
    dateLastChanged
    corpLevelFlag
    partner{
      partnerId
      partnerName
    }
    builderId
    builders {
      builderId
      builderName
      statusId
      planLibraryType
      divisionsCount
      diagnostic
      publishReviews
      autoFeedFlag
      displayChannelSelection {
        displayOnWebArch
      }
      partner{
        partnerName
      }
      builderLogo
      divisions {
        builderId
        builderName
        statusId
        orgTypeCode
        partner{
          partnerId
        }
        displayChannelSelection {
          displayOnWebArch
        }
      }
    }
    communities {
      id
      name
      status
      specs(sortBy: [{order: Asc, field: Number}], status: [Active, Inactive]) {
        id
        number
      }
      plans(sortBy : [{order: Asc, field: Name}], status: [Active, Inactive]) {
        id
        name
      }
    }
    userPermissions {
      accessCode
      displayName
      groupId
      id
      isActive
      name
    }
    services {
      id
      name
      value
    }
    portalProducts {
      groupDisplayOrder
      groupName
      participationFlag
      productDisplayOrder
      productName
    }
    contacts {
      contact
      imagePath
      primaryEmail
      primaryPhone
      productGroupName
      productName
      type
    }
    dDRMonthlyApproval {
      period
    }
  }
}`;
