const fs = require('fs');
const { indexBy } = require('../../utils/index-by');
const { userInfoQuery } = require('./query');
const { login, graphRequest } = require('./requests');

module.exports.login = async (url, accessToken, { filename }, opts) => {
  if (opts && opts.access) {
    console.log(accessToken);
    return;
  }

  const loginResult = await login(url, accessToken).catch((error) => {
    console.error(error);
  });
  if (!loginResult) return;

  const {
    token, UserId,
  } = loginResult;

  if (opts && opts.token) {
    console.log(token, UserId);
    return;
  }

  const data = await graphRequest(url, { token, query: userInfoQuery(UserId) }) || [];

  const [{ userPermissions, services, partner } = {}] = data.users;
  let [{ communities, builders } = {}] = data.users;

  const user = data.users[0];

  // Partner calculus
  user.partner = partner && partner.partnerId;
  const partners = indexBy([partner], 'partnerId');

  // Division calculus
  let divisions = [];
  builders = (builders || []).map((builder) => {
    divisions.push(...builder.divisions.map((division) => {
      division.partner = division.partner && division.partner.partnerId;
      return division;
    }));
    builder.partner = builder.partner && builder.partner.partnerId;
    builder.divisions = builder.divisions && builder.divisions.map(({ builderId }) => builderId);
    return builder;
  });
  const divisionId = (divisions && divisions[0] && divisions[0].builderId) || null;
  divisions = indexBy(divisions || [], 'builderId');

  // Builder calculus
  user.builders = builders && builders.map(({ builderId }) => builderId);
  builders = indexBy(builders || [], 'builderId');

  // Community calculus
  user.communities = communities && communities.map(({ id }) => id);
  communities = indexBy(communities || [], 'builderId');

  // Remove unnecessary
  delete user.userPermissions;
  delete user.services;

  // Calculate properties
  user.fullName = `${user.firstName} ${user.lastName}`;

  const store = {
    app: {
      success: JSON.stringify({
        'BDXLIVE/AUTH/LOGIN': true,
      }),
      loadings: JSON.stringify({
        'BDXLIVE/AUTH/LOGIN': false,
      }),
      authentication: JSON.stringify({
        user: UserId,
        userPermissions: userPermissions.map(({ id }) => id),
        services: services.map(({ id }) => id),
      }),
      entities: JSON.stringify({
        users: {
          [UserId]: user,
        },
        partners,
        builders,
        divisions,
        communities,
        userPermissions: indexBy(userPermissions, 'id'),
        services: indexBy(services, 'id'),
      }),
      navigation: JSON.stringify({
        global: {
          nestedComboBox: {},
          communityTreeViewFilter: 'Approved',
          locationForced: null,
          partner: user.partner,
          builder: user.builderId,
          division: divisionId,
          community: null,
          plan: null,
          spec: null,
        },
      }),
      _persist: JSON.stringify({
        rehydrated: false,
        version: -1,
      }),
    },
    token,
  };
  fs.writeFileSync(`${filename}.json`, JSON.stringify(store), 'utf8');
};
