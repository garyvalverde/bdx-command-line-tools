const fetch = require('node-fetch');
const { jwtDecode } = require('../../utils/jwt');

module.exports.login = async (url, accessToken) => {
  const response = await fetch(`${url}Authentication`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(accessToken),
  });
  const data = await response.json();
  const error = data.LastErrorMessage;
  const token = data.Result.data;

  if (error) throw new Error(error);

  return { token, ...jwtDecode(token, 1) };
};

module.exports.graphRequest = async (url, { token, query }) => {
  const response = await fetch(url, {
    method: 'POST',
    headers: {
      Authorization: token,
      'Content-Type': 'application/json',
    },
    body: JSON.stringify({
      query,
    }),
  });
  const data = await response.json();
  return data.Result;
};
