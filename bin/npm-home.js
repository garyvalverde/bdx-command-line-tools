#!/usr/bin/env node --harmony
const chalk = require('chalk');
const packageJson = require('package-json');
const githubUrlFromGit = require('github-url-from-git');
const isUrl = require('is-url-superb');
const { any, npm, yarn } = require('./utils/open-page');

module.exports = function npmHome(comm) {
  comm
    .command('nh [name]')
    .option('-g, --github', 'Opens the github documentation')
    .option('-y, --yarn', 'Opens the yarn documentation')
    .action((name, opts) => {
      if (opts && opts.github) {
        return packageJson(name, { fullMetadata: true }).then(({ homepage = '', repository = {} }) => {
          if (repository) {
            let gitUrl = githubUrlFromGit(repository.url);

            if (!gitUrl) {
              gitUrl = repository.url;

              if (isUrl(gitUrl) && /^https?:\/\//.test(gitUrl)) {
                console.error(`The \`repository\` field in package.json should point to a Git repo and not a website. Please open an issue or pull request on \`${name}\`.`);
              } else {
                console.error(`The \`repository\` field in package.json is invalid. Please open an issue or pull request on \`${name}\`. Using the \`homepage\` field instead.`);

                gitUrl = homepage;
              }
            }

            return any(gitUrl, { wait: false });
          }

          return npm(name);
        }).catch((err) => {
          if (err.code === 'ENOTFOUND') {
            console.error(chalk.red('No network connection detected!'));
            process.exit(1);
          }
          console.error(chalk.red(err));
        });
      }

      if (opts && opts.yarn) {
        return yarn(name);
      }

      return npm(name);
    });
};
