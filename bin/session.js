#!/usr/bin/env node --harmony
const { promptSessionLogin } = require('./lib/init');

module.exports = function code(comm) {
  comm
    .command('session')
    .option('-a, --access', 'Only returns the user access')
    .option('-t, --token', 'Only returns the user token')
    .action((opts) => {
      promptSessionLogin(opts);
    });
};
