#!/usr/bin/env node --harmony
const { promptGeneratorSelection } = require('./lib/init');

module.exports = function scaffolding(comm) {
  comm
    .command('add')
    .action(() => {
      promptGeneratorSelection();
    });
};
