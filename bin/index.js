const code = require('./code');
const short = require('./short');
const session = require('./session');
const npmHome = require('./npm-home');
const scaffolding = require('./scaffolding');

module.exports = {
  code, short, npmHome, scaffolding, session,
};
