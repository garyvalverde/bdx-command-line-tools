const path = require('path');
const findUp = require('find-up');

module.exports = class FindRelative {
  constructor(location) {
    this.location = location;
  }

  find(fileName, ext = '') {
    const absolutePath = findUp.sync(fileName + ext);
    if (!absolutePath) throw new Error(`Unable to find ${fileName}${ext}`);
    const relativePath = path.relative(this.location, absolutePath).replace(/\\/g, '/').replace(ext, '');
    return relativePath;
  }
};
