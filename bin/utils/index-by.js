module.exports.indexBy = function indexBy(list, iteratee, context) {
  return list.reduce((map, obj) => {
    const key = typeof iteratee === 'string' ? obj[iteratee] : iteratee.call(context, obj);
    map[key] = obj;
    return map;
  }, {});
};
