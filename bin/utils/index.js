const { prompt, promptList } = require('./prompt');
const getCurrentLocation = require('./get-current-location');

module.exports = { prompt, promptList, getCurrentLocation };
