const opn = require('opn');

module.exports = {
  any: (page) => opn(page, { wait: false }),
  npm: (name) => opn(`https://www.npmjs.com/package/${name}`, { wait: false }),
  yarn: (name) => opn(`https://yarn.pm/${name}`, { wait: false }),
};
