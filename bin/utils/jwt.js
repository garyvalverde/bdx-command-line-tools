module.exports.jwtDecode = (token, index) => {
  const base64Url = token.split('.')[index];
  const base64 = base64Url.replace('-', '+').replace('_', '/');
  return JSON.parse(Buffer.from(base64, 'base64').toString('binary'));
};
