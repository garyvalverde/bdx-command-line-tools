const cryptojs = require('crypto-js');

/**
 * Generate random int
 */
const getRandom16DigitsInt = () => {
  const factor = 1000000000000000;
  return parseInt(String(factor + Math.random() * 9 * factor), 10);
};

module.exports.generateToken = (value) => {
  const key = getRandom16DigitsInt();
  const iv = getRandom16DigitsInt();

  const text = cryptojs.enc.Utf8.parse(JSON.stringify(value));

  const encrypted = cryptojs.AES.encrypt(text, cryptojs.enc.Utf8.parse(key), {
    keySize: 128 / 8,
    iv: cryptojs.enc.Utf8.parse(iv),
    mode: cryptojs.mode.CBC,
    padding: cryptojs.pad.Pkcs7,
  });

  const encodedEncrypted = Buffer.from(`${key}.${iv}.${encrypted.toString()}`).toString('base64');
  return encodedEncrypted;
};
