const inquirer = require('inquirer');

module.exports.promptList = async function promptList(question) {
  const rawQuestion = {
    type: 'list',
    name: 'answers',
    message: question.message,
    choices: question.choices,
  };

  const { answers } = await inquirer.prompt([rawQuestion]);
  return answers;
};

module.exports.prompt = async function prompt(questions) {
  return inquirer.prompt(questions);
};
