#!/usr/bin/env node --harmony
const chalk = require('chalk');
const shorturl = require('shorturl-2');

module.exports = function short(comm) {
  comm
    .command('short [url]')
    .action((url) => {
      shorturl(url, (shortUrl) => {
        console.log(chalk.blue(shortUrl));
      });
    });
};
