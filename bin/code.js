#!/usr/bin/env node --harmony
const { exec } = require('child_process');

module.exports = function code(comm) {
  comm
    .command('code [directory]')
    .option('-n, --new', 'Opens the directory on a new workspace')
    .action((directory, opts) => {
      let cmd = 'code ';
      // Add directory
      cmd += directory ? `"${directory}"` : '.';
      // Add params
      cmd += opts && opts.new ? '' : ' --add';

      exec(cmd);
    });
};
